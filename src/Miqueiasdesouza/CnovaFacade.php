<?php

namespace Miqueiasdesouza\Cnova;

use Illuminate\Support\Facades\Facade;

class CnovaFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'cnova';
    }
}
