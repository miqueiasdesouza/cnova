<?php

namespace Miqueiasdesouza\Cnova;

use Illuminate\Support\ServiceProvider;

class CnovaServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('cnova', function ($app) {
            return new Cnova($app['session'], $app['config'], $app['log'], $app['validator']);
        });
    }

    public function boot()
    {

    }
}
